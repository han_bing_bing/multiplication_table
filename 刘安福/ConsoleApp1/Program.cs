﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //九九乘法表
            for(int i = 1; i< 10; i++)
            {
                for(int j = 1; j <= i; j++)
                {
                    Console.Write("{0}×{1}="+i*j+"\t",j, i);
                   // Console.Write("{0}×{1}={2}\t",j,i,i*j);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine();
            //循环
            string str= "祖国，你好";
            for(int i = 0; i <str.Length; i++)
            {
                Console.WriteLine(str[i]);
            }
            Console.WriteLine();

            //菱形
            Console.WriteLine("请输入行数");//用户输入几行

            for (int i = 1; i <=3; i++)
            {
                for(int j = 1; j <= 3-i; j++)
                {
                    Console.Write("  ");//两空格
                }
                for(int k = 1; k <= (2*i)-1; k++)
                {
                    Console.Write("☆");
                }
                Console.WriteLine();
            }
            for(int i = 1; i <= 3; i++)
            {
                for(int j = 1; j <= i; j++)
                {
                    Console.Write("  ");//两空格
                }
                for (int k = 1; k <= 5-2*i; k++)
                {
                    Console.Write("☆");
                }
                Console.WriteLine();
            }


        }
    }
}
