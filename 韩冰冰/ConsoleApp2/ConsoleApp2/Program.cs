﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            for(int i = 1;i < 10; i++)
            {
                for(int j = 1; j <= i; j++)
                {
                    Console.Write("{0} X {1} = {2} \t",i,j,i*j);
                }
                Console.WriteLine();
            }
        }
    }
}
