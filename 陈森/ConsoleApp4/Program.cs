﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 9; i++)
            {
                for (int k = 1; k <= i; k++)
                {
                    Console.Write(i + "*" + k + "=" + i * k + "\t");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
